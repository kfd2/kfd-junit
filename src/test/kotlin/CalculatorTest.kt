import org.junit.jupiter.api.Assumptions.assumeFalse
import org.junit.jupiter.api.Assumptions.assumeTrue
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows
import java.util.*
import kotlin.test.assertEquals

class CalculatorTest {

    private val calculator = Calculator()

    @Test
    fun `test number sum`() {
        val a = 5
        val b = 15
        assertEquals(a + b, calculator.sum(a, b))
    }

    @TestFactory
    fun `test number mul`(): Iterable<DynamicTest> {
        val nums = List(1) { it - 5 }
        return nums.map { num ->
            List(11) { it - 5 }.map {
                dynamicTest("Test $num * $it") {
                    assertEquals(num * it, calculator.mul(num, it))
                }
            }
        }.flatten()
    }

    @TestFactory
    fun `say hello to users`(): Iterable<DynamicTest> {
        return List(11) { UUID.randomUUID() }.map {
            val name = it.toString()
            dynamicTest("Test hello to user $name") {
                assertEquals("Hello, $name", calculator.sayHello(name))
            }
        }
    }

    @Test
    fun `say hello to John`() {
        val name = "John"
        assertThrows<Exception> { calculator.sayHello(name) }
    }

    @Test
    fun `is working?`() {
        assumeTrue(calculator.initialized)
        assertEquals("Working", calculator.isInitialized())
    }

    @Test
    fun `is not working?`() {
        assumeFalse(calculator.initialized)
        assertEquals("Wait...", calculator.isInitialized())
    }
}
