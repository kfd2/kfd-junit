class Calculator {
    var initialized = true

    fun isInitialized() = if (initialized) "Working" else "Wait..."

    fun sum(a: Int, b: Int) = a + b

    fun mul(a: Int, b: Int) = a * b

    fun sayHello(name: String): String {
        if (name != "John") return "Hello, $name"
        throw Exception("John is forbidden to use this app")
    }
}